package server

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	admissionv1 "k8s.io/api/admission/v1"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/fake"
	k8sTesting "k8s.io/client-go/testing"
)

var DummyEnvvarsPrefix = "some.random.prefix.v1."

func decodeResponse(body io.ReadCloser) (*admissionv1.AdmissionReview, error) {
	response, _ := io.ReadAll(body)
	review := &admissionv1.AdmissionReview{}
	decoder := serializer.NewCodecFactory(runtime.NewScheme()).UniversalDeserializer()
	_, _, err := decoder.Decode(response, nil, review)
	return review, err
}

func encodeRequest(review *admissionv1.AdmissionReview) []byte {
	ret, err := json.Marshal(review)
	if err != nil {
		logrus.Errorln(err)
	}
	return ret
}

func getResponse(request admissionv1.AdmissionReview, fakeK8sClient kubernetes.Interface, extraEnvvars []ExtraEnvvar) (*admissionv1.AdmissionReview, error) {
	admission := &EnvvarsAdmission{
		K8sClient:     fakeK8sClient,
		EnvvarsPrefix: DummyEnvvarsPrefix,
		ExtraEnvvars:  extraEnvvars,
	}

	server := httptest.NewServer(GetAdmissionControllerServerNoSsl(admission, ":8080").Handler)
	requestString := string(encodeRequest(&request))
	myr := strings.NewReader(requestString)
	r, _ := http.Post(server.URL, "application/json", myr)
	return decodeResponse(r.Body)
}

func getDummyAdmissionReview() admissionv1.AdmissionReview {
	return admissionv1.AdmissionReview{
		TypeMeta: v1.TypeMeta{
			Kind: "AdmissionReview",
		},
		Request: &admissionv1.AdmissionRequest{
			UID: "e911857d-c318-11e8-bbad-025000000001",
			Kind: v1.GroupVersionKind{
				Group: "", Version: "v1", Kind: "pod",
			},
			Operation: "CREATE",
			Namespace: "tool-test",
			Object: runtime.RawExtension{
				Raw: []byte(`{
					"kind": "Pod",
					"apiVersion": "v1",
					"metadata": {
						"name": "test-123123123",
						"namespace": "tool-test",
						"uid": "4b54be10-8d3c-11e9-8b7a-080027f5f85c",
						"creationTimestamp": "2019-06-12T18:02:51Z"
					},
					"spec": {
						"nodeSelector": {
							"foo": "bar"
						},
						"containers": [
							{
								"name": "test",
								"image": "docker-registry.tools.wmflabs.org/toolforge-python39-web:latest",
								"command": ["/usr/bin/webservice-runner"],
								"args": ["python39"]
							}
						]
					}
				}`),
			},
		},
	}
}

func getDummyRequest(namespace string, env []byte, volumeMounts []byte, volumes []byte) *admissionv1.AdmissionRequest {
	if namespace == "" {
		namespace = "tool-test"
	}
	header := []byte(`{
				"kind": "Pod",
				"apiVersion": "v1",
				"metadata": {
					"name": "maintain-kubeusers-123123123",
					"namespace": "maintain-kubeusers",
					"uid": "4b54be10-8d3c-11e9-8b7a-080027f5f85c",
					"creationTimestamp": "2019-06-12T18:02:51Z"
				},
				"spec": {`)
	if volumes == nil {
		volumes = []byte(`
					"volumes": [
						{
							"name": "home",
							"hostPath": {
								"path": "/data/project",
								"type": "Directory"
							}
						}
					],`)
	}
	containers_header := []byte(`
					"containers": [
						{
							"name": "maintain-kubeusers",
							"image": "docker-registry.tools.wmflabs.org/maintain-kubeusers:latest",
							"workingDir": "/some/path",
							"command": ["/app/venv/bin/python"],`)
	if env == nil {
		env = []byte(`
							"env": [
								{
									"name": "NO_HOME",
									"value": "original value"
								}
							],`)
	}
	if volumeMounts == nil {
		volumeMounts = []byte(`
							"volumeMounts": [
								{
									"mountPath": "/data/project",
									"name": "home"
								}
							],`)
	}
	containers_footer := []byte(`
							"args": ["/app/maintain_kubeusers.py"]
						}
					]`)
	footer := []byte(`
				}
			}`)
	return &admissionv1.AdmissionRequest{
		UID: "e911857d-c318-11e8-bbad-025000000001",
		Kind: v1.GroupVersionKind{
			Group: "", Version: "v1", Kind: "pod",
		},
		Operation: "CREATE",
		Namespace: namespace,
		Object: runtime.RawExtension{
			Raw: append(
				header, append(
					volumes, append(
						containers_header, append(
							env, append(
								volumeMounts, append(
									containers_footer,
									footer...)...)...)...)...)...),
		},
	}
}

func assertAllowedAndGetPatch(review *admissionv1.AdmissionReview, err error, t *testing.T) []PatchOperation {
	t.Log(review.Response)

	if err != nil {
		t.Error(err)
	}

	if !review.Response.Allowed {
		t.Error("Should not disallow tools with no HOME")
	}

	if *review.Response.PatchType != admissionv1.PatchTypeJSONPatch {
		t.Error("Wrong patch type found")
	}

	var patchOp []PatchOperation
	err = json.Unmarshal(review.Response.Patch, &patchOp)
	if err != nil {
		t.Error(err)
	}
	return patchOp
}

func TestServerIgnoresNonToolPods(t *testing.T) {
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: []corev1.Secret{
				{
					ObjectMeta: v1.ObjectMeta{
						Name: "dummy-secret-1",
					},
					Type: corev1.SecretTypeBasicAuth,
					StringData: map[string]string{
						"DUMMY_SECRET_1": "Some extra secret data for dummy secret 1",
					},
				},
			},
		}, nil
	})
	review, err := getResponse(admissionv1.AdmissionReview{
		TypeMeta: v1.TypeMeta{Kind: "AdmissionReview"},
		Request:  getDummyRequest("maintain-kubeusers", []byte(`"env": [],`), []byte(``), []byte(``)),
	}, &fakeK8s, nil)

	t.Log(review.Response)

	if err != nil {
		t.Error(err)
	}

	if review.Response.Allowed {
		t.Error("Should not allow non-tools")
	}

	if review.Response.Patch != nil {
		t.Error("Should not contain patch when not allowed")
	}
}

func TestServerAddsAllEnvvarsWhenNoneExistIfPrefixOk(t *testing.T) {
	secrets := []corev1.Secret{
		{
			ObjectMeta: v1.ObjectMeta{
				Name:   fmt.Sprintf("%sdummy-secret-1", DummyEnvvarsPrefix),
				Labels: map[string]string{"app": "toolforge"},
			},
			Type: corev1.SecretTypeBasicAuth,
			StringData: map[string]string{
				"DUMMY_SECRET_1": "Some extra secret data for dummy secret 1",
			},
		},
	}
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: secrets,
		}, nil
	})
	review, err := getResponse(getDummyAdmissionReview(), &fakeK8s, nil)

	var gottenPatch = assertAllowedAndGetPatch(review, err, t)

	// Plus one due to the patch initializing the env array
	if len(gottenPatch) != len(secrets)+1 {
		t.Fatalf("Patch length %d does not match expected value %d", len(gottenPatch), len(secrets)+1)
	}
}

func TestServerAddsNoEnvvarsWhenNoneExistIfWrongLabels(t *testing.T) {
	secrets := []corev1.Secret{
		{
			ObjectMeta: v1.ObjectMeta{
				Name:   fmt.Sprintf("%sdummy-secret-1", DummyEnvvarsPrefix),
				Labels: map[string]string{"app": "not toolforge"},
			},
			Type: corev1.SecretTypeBasicAuth,
			StringData: map[string]string{
				"DUMMY_SECRET_1": "Some extra secret data for dummy secret 1",
			},
		},
	}
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: secrets,
		}, nil
	})
	review, err := getResponse(getDummyAdmissionReview(), &fakeK8s, nil)

	var gottenPatch = assertAllowedAndGetPatch(review, err, t)

	if len(gottenPatch) != 1 {
		t.Fatalf("Patch length %d does not match expected value 1: %v", len(gottenPatch), gottenPatch)
	}
}

func TestServerAddsNoEnvvarsWhenNoneExistIfPrefixNotOk(t *testing.T) {
	secrets := []corev1.Secret{
		{
			ObjectMeta: v1.ObjectMeta{
				Name:   "some-non-matching-prefix.dummy-secret-1",
				Labels: map[string]string{"app": "toolforge"},
			},
			Type: corev1.SecretTypeBasicAuth,
			StringData: map[string]string{
				"DUMMY_SECRET_1": "Some extra secret data for dummy secret 1",
			},
		},
	}
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: secrets,
		}, nil
	})
	review, err := getResponse(getDummyAdmissionReview(), &fakeK8s, nil)

	var gottenPatch = assertAllowedAndGetPatch(review, err, t)

	if len(gottenPatch) != 1 {
		t.Fatalf("Patch length %d does not match expected value 1: %v", len(gottenPatch), gottenPatch)
	}
}

func TestServerReturnsRightEnvvar(t *testing.T) {
	secrets := []corev1.Secret{
		{
			ObjectMeta: v1.ObjectMeta{
				Name:   fmt.Sprintf("%sdummy-secret-1", DummyEnvvarsPrefix),
				Labels: map[string]string{"app": "toolforge"},
			},
			Type: corev1.SecretTypeBasicAuth,
			StringData: map[string]string{
				"DUMMY_SECRET_1": "Some extra secret data for dummy secret 1",
			},
		},
	}
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: secrets,
		}, nil
	})
	expectedEnvVar := map[string]interface{}{
		"name": "DUMMY_SECRET_1",
		"valueFrom": map[string]interface{}{
			"secretKeyRef": map[string]interface{}{
				"name": fmt.Sprintf("%sdummy-secret-1", DummyEnvvarsPrefix),
				"key":  "DUMMY_SECRET_1",
			},
		},
	}

	review, err := getResponse(getDummyAdmissionReview(), &fakeK8s, nil)

	var gottenPatch = assertAllowedAndGetPatch(review, err, t)
	// the first patch is to add an empty env array to the container
	if len(gottenPatch) != len(secrets)+1 {
		t.Fatalf("Patch length %d does not match expected value %d", len(gottenPatch), len(secrets))
	}

	gottenVars := gottenPatch[1].Value.(map[string]interface{})

	if !reflect.DeepEqual(expectedEnvVar, gottenVars) {
		t.Fatalf("Expected new env var to be:\n%v\nBut got:\n%v", expectedEnvVar, gottenVars)
	}
}

func TestServerReturnsRightOpAndPath(t *testing.T) {
	secrets := []corev1.Secret{
		{
			ObjectMeta: v1.ObjectMeta{
				Name:   fmt.Sprintf("%sdummy-secret-1", DummyEnvvarsPrefix),
				Labels: map[string]string{"app": "toolforge"},
			},
			Type: corev1.SecretTypeBasicAuth,
			StringData: map[string]string{
				"DUMMY_SECRET_1": "Some extra secret data for dummy secret 1",
			},
		},
	}
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: secrets,
		}, nil
	})
	review, err := getResponse(getDummyAdmissionReview(), &fakeK8s, nil)

	var gottenPatch = assertAllowedAndGetPatch(review, err, t)

	// the first patch is to add an empty env array to the container
	if len(gottenPatch) != len(secrets)+1 {
		t.Fatalf("Patch length %d does not match expected value %d", len(gottenPatch), len(secrets))
	}

	if gottenPatch[1].Op != "add" {
		t.Fatalf("Expected patch to be an add operation, got %s", gottenPatch[0].Op)
	}
	if gottenPatch[1].Path != "/spec/containers/0/env/-" {
		t.Fatalf("Expected patch to be on path /spec/containers/0/env/-, got %s", gottenPatch[0].Path)
	}
}

func TestServerReturnsExtraEnvvar(t *testing.T) {
	secrets := []corev1.Secret{}
	fakeK8s := fake.Clientset{}
	fakeK8s.AddReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, &corev1.SecretList{
			Items: secrets,
		}, nil
	})

	expectedEnvVar := map[string]interface{}{
		"name":  "DUMMY_EXTRA_ENVVAR_1",
		"value": "dummy-extra-envvar-1-value",
	}

	review, err := getResponse(getDummyAdmissionReview(), &fakeK8s, []ExtraEnvvar{{Name: "DUMMY_EXTRA_ENVVAR_1", Value: "dummy-extra-envvar-1-value"}})

	var gottenPatch = assertAllowedAndGetPatch(review, err, t)
	// the first patch is to add an empty env array to the container
	if len(gottenPatch) != 2 {
		t.Fatalf("Patch length %d does not match expected value %d", len(gottenPatch), len(secrets))
	}

	gottenVars := gottenPatch[1].Value.(map[string]interface{})

	if !reflect.DeepEqual(expectedEnvVar, gottenVars) {
		t.Fatalf("Expected new env var to be:\n%v\nBut got:\n%v", expectedEnvVar, gottenVars)
	}
}
