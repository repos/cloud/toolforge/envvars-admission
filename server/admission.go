package server

import (
	"context"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	admissionv1 "k8s.io/api/admission/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/json"
	"k8s.io/client-go/kubernetes"
)

// PatchOperation describes an operation done to modify a Kubernetes
// resource
type PatchOperation struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

type ExtraEnvvar struct {
	Name  string `env:"NAME"`
	Value string `env:"VALUE"`
}

// EnvvarsAdmission type is what does all the magic
type EnvvarsAdmission struct {
	K8sClient     kubernetes.Interface
	EnvvarsPrefix string
	ExtraEnvvars  []ExtraEnvvar
}

func fromK8sName(name string, prefix string) string {
	return strings.ReplaceAll(strings.ToUpper(name[len(prefix):]), "-", "_")
}

// HandleAdmission has all the webhook logic to possibly add envvars
// to containers if needed
func (admission *EnvvarsAdmission) HandleAdmission(review *admissionv1.AdmissionReview) {
	req := review.Request

	var pod corev1.Pod
	err := json.Unmarshal(req.Object.Raw, &pod)
	if err != nil {
		logrus.Errorf("Could not unmarshal raw object: %v", err)
		review.Response = &admissionv1.AdmissionResponse{
			UID: review.Request.UID,
			Result: &metav1.Status{
				Message: err.Error(),
			},
			Allowed: false,
		}

		return
	}

	logrus.Debugf("Envvars AdmissionReview for Kind=%v, Namespace=%v Name=%v (%v) UID=%v patchOperation=%v UserInfo=%v",
		req.Kind, req.Namespace, req.Name, pod.Name, req.UID, req.Operation, req.UserInfo)

	if !strings.HasPrefix(req.Namespace, "tool-") {
		logrus.Warningf("Skipping non-tool namespace %v", req.Namespace)

		review.Response = &admissionv1.AdmissionResponse{
			UID: review.Request.UID,
			Result: &metav1.Status{
				Message: "Only tools mount the secrets in them.",
			},
			Allowed: false,
		}

		return
	}

	secrets, err := admission.K8sClient.CoreV1().Secrets(req.Namespace).List(context.TODO(), metav1.ListOptions{
		LabelSelector: "app=toolforge", Limit: 100,
	})

	if err != nil {
		logrus.Errorf("Error trying to list secrets on namespace %s: %s", req.Namespace, err)
		review.Response = &admissionv1.AdmissionResponse{
			UID: review.Request.UID,
			Result: &metav1.Status{
				Message: fmt.Sprintf("Got error when trying to get the secrets from namespace %s", req.Namespace),
			},
			Allowed: false,
		}
	}

	var patchOperation []PatchOperation
	for i, container := range pod.Spec.Containers {
		// If there is no env, json-patch will fail
		// unless we add it with an op.
		if len(container.Env) == 0 {
			patch := PatchOperation{
				Op:    "add",
				Path:  fmt.Sprintf("/spec/containers/%d/env", i),
				Value: []string{},
			}
			patchOperation = append(patchOperation, patch)
		}
	}

	logrus.Infof("Got secrets: %v", secrets.Items)

	for _, secret := range secrets.Items {
		if !strings.HasPrefix(secret.Name, admission.EnvvarsPrefix) {
			logrus.Debugf(
				"Skipping secret %s of namespace %s as it does not begin with %s", secret.Name, req.Namespace, admission.EnvvarsPrefix,
			)
			continue
		}
		envVarShellName := fromK8sName(secret.Name, admission.EnvvarsPrefix)

		for i := range pod.Spec.Containers {
			patch := PatchOperation{
				Op:   "add",
				Path: fmt.Sprintf("/spec/containers/%d/env/-", i),
				Value: &corev1.EnvVar{
					Name: envVarShellName,
					ValueFrom: &corev1.EnvVarSource{
						SecretKeyRef: &corev1.SecretKeySelector{
							LocalObjectReference: corev1.LocalObjectReference{
								Name: secret.Name,
							},
							Key: envVarShellName,
						},
					},
				},
			}
			patchOperation = append(patchOperation, patch)
		}
	}

	// doing this after the secrets overrides any envvars set by the user
	for _, extraEnvvar := range admission.ExtraEnvvars {
		for i := range pod.Spec.Containers {
			patch := PatchOperation{
				Op:   "add",
				Path: fmt.Sprintf("/spec/containers/%d/env/-", i),
				Value: &corev1.EnvVar{
					Name:  extraEnvvar.Name,
					Value: extraEnvvar.Value,
				},
			}
			patchOperation = append(patchOperation, patch)
		}
	}

	patchType := admissionv1.PatchTypeJSONPatch
	response := &admissionv1.AdmissionResponse{
		UID:       review.Request.UID,
		Allowed:   true,
		Result:    &metav1.Status{Message: "Envvars mounted"},
		PatchType: &patchType,
	}

	// parse the []map into JSON
	response.Patch, err = json.Marshal(patchOperation)
	if err != nil {
		logrus.Errorf("Could not marshal patch object: %v", err)
		review.Response = &admissionv1.AdmissionResponse{
			UID: review.Request.UID,
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}

		return
	}

	review.Response = response
}
