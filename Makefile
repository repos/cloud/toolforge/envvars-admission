SHELL := /bin/bash

# not generating any local files
.PHONY: run image build kind_load rollout build-and-deploy-local

build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -buildvcs=false -a -installsuffix cgo -ldflags="-w -s" -o envvars-admission .

image:
	bash -c "source <(minikube docker-env) || : && docker build --target image -f .pipeline/blubber.yaml . -t envvars-admission:dev"

kind_load:
	bash -c "hash kind 2>/dev/null && kind load docker-image docker.io/library/envvars-admission:dev --name toolforge || :"

rollout:
	kubectl rollout restart -n envvars-admission deployment envvars-admission

build-and-deploy-local: image kind_load rollout
	./deploy.sh local
