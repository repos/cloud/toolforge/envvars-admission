apiVersion: apps/v1
kind: Deployment
metadata:
  name: envvars-admission
  labels:
    name: envvars-admission
  annotations:
    secret.reloader.stakater.com/reload: {{ .Values.webhook.secretName }}
spec:
  replicas: {{ .Values.replicas }}
  selector:
    matchLabels:
      name: envvars-admission
  template:
    metadata:
      name: envvars-admission
      labels:
        name: envvars-admission
    spec:
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels:
              name: envvars-admission
      containers:
        - name: webhook
          image: "{{ .Values.image.name }}:{{ .Values.image.tag }}"
          imagePullPolicy: "{{ .Values.image.pullPolicy }}"
          env:
            - name: "DEBUG"
              value: "{{ .Values.config.debug }}"
            - name: "ENVVARS_PREFIX"
              value: "{{ .Values.config.envvarsPrefix }}"
            {{- range $index, $envvar := .Values.config.extraEnvvars }}
            - name: "EXTRA_{{ $index }}_NAME"
              value: "{{ $envvar.name }}"
            - name: "EXTRA_{{ $index }}_VALUE"
              value: "{{ $envvar.value }}"
            {{- end }}
          resources: {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: webhook-certs
              mountPath: /etc/webhook/certs
              readOnly: true
          securityContext:
            readOnlyRootFilesystem: true
      volumes:
        - name: webhook-certs
          secret:
            secretName: "{{ .Values.webhook.secretName }}"
      serviceAccountName: envvars-admission
