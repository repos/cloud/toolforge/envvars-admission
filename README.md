# envvars-admission-controller

Automatically adds toolforge secrets as envvars for
[Toolforge](https://toolforge.org) pods, and sets some global envvars for
everyone.

See the `deployment/chart/values.yaml` file for the configuration options.

## Deploying to Toolforge

See the
[toolforge deploy instructions](https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy#deploying-on-toolsbeta-and-toolforge).

## Local development

1. Start a local Toolforge cluster (you can use minikube, kind or
   [lima-kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo/)).

2. Build the Docker image locally and deploy (if minikube is installed it will
   use minikube, otherwise will use kind)

   ```shell-session
   $ make build-and-deploy-local
   ```
