package main

import (
	"github.com/caarlos0/env/v11"
	"github.com/sirupsen/logrus"
	"gitlab.wikimedia.org/cloud/toolforge/envvars-admission-controller/server"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Config contains the general configuration of the webhook via env variables
type Config struct {
	ListenOn      string               `env:"LISTEN_ON" envDefault:"0.0.0.0:8080"`
	TLSCert       string               `env:"TLS_CERT" envDefault:"/etc/webhook/certs/tls.crt"`
	TLSKey        string               `env:"TLS_KEY" envDefault:"/etc/webhook/certs/tls.key"`
	Debug         bool                 `env:"DEBUG" envDefault:"true"`
	EnvvarsPrefix string               `env:"ENVVARS_PREFIX" envDefault:"toolforge.envvar.v1."`
	ExtraEnvvars  []server.ExtraEnvvar `envPrefix:"EXTRA"`
}

func main() {
	config := &Config{}
	err := env.Parse(config)
	if err != nil {
		logrus.Fatalf("Could not load envconfig: %v", err)
	}
	logrus.Infof("Config: %v", config)

	if config.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	k8sConfig, err := rest.InClusterConfig()
	if err != nil {
		logrus.Fatalln(err.Error())
	}
	clientset, err := kubernetes.NewForConfig(k8sConfig)
	if err != nil {
		logrus.Fatalln(err.Error())
	}
	envvarsAdmission := &server.EnvvarsAdmission{
		K8sClient:     clientset,
		EnvvarsPrefix: config.EnvvarsPrefix,
		ExtraEnvvars:  config.ExtraEnvvars,
	}

	s, err := server.GetAdmissionControllerServer(envvarsAdmission, config.TLSCert, config.TLSKey, config.ListenOn)
	if err != nil {
		logrus.Fatalf("Could not create server instance: %v", err)
	}

	logrus.Infof("Starting web server on %v", config.ListenOn)
	err = s.ListenAndServeTLS("", "")
	if err != nil {
		logrus.Fatalf("Could not start web server: %v", err)
	}
}
